package org.example;

import java.net.MalformedURLException;
import java.net.URL;

public class Main {

    public static  void main(String[] args) {
        try {
            Extraction extract = new Extraction();
            URL url;
            String[] apiWord = new String[7];
            apiWord[0] = "halifax";
            apiWord[1] = "Canada";
            apiWord[2] = "University";
            apiWord[3] = "Dalhousie";
            apiWord[4] = "Canada%20Education";
            apiWord[5] = "Moncton";
            apiWord[6] = "Toronto";

            for (String word : apiWord) {
                url = new URL("https://newsapi.org/v2/everything?q=" + word + "&apiKey=083596f1a85749ba83b818dfb307376d&pageSize=100");
                extract.getNews(url);
                extract.passNews();

            }
        }
        catch(MalformedURLException ei)
        {
            ei.printStackTrace();
        }
        Extraction extract=new Extraction();
        extract.cleanData ();

    }

}